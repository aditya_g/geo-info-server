export const generateTableHeaders = (columns) => {
    return (<thead>
            <tr>
                <th>#</th>
                {getTableHeaderRows(columns)}
            </tr>
        </thead>
    )
}

export const generateTableBody = data => {
   return( !data ?  
        <div style={{padding : '10px', textAlign: 'center'}}><i> No records found</i></div> :
        <tbody>
            {getTableBodyRows(data)}
        </tbody>
    )}

const getTableHeaderRows = cols => cols.map(item => <th>{item}</th>);

const getTableBodyRows = data => data.map((item, index) => <tr>
    <td>{index}</td>
    {getTableRows(item)}
</tr>)

const getTableRows = item => {
    var temp = []
   for(var [key,value] of Object.entries(item)){
    temp.push(<td>{value}</td>)
   }
   return temp;
}
