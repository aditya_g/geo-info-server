import {useState, useEffect} from 'react';
import { generateTableHeaders,generateTableBody } from './helpers';

export const useTable = (col, data) => {
    const [columns, setColumns] = useState(col);
    const [tableHeaders, setTableHeaders] = useState([]);
    const [tableRows, setTableRows] = useState([]);

    useEffect(() => {
        setColumns(col);
        setTableHeaders(generateTableHeaders(col))
    },[]);

    useEffect(() => {       
       data.length && setTableRows(generateTableBody(data))
    },[data]);

    return [
        columns,
        tableHeaders,
        tableRows
    ]
}