import { Table } from 'react-bootstrap';
import { useTable } from './hooks';
import {columns} from './constants';

export const TableContainer = ({data}) => {
  const [col,
    tableHeaders,
    tableRows
  ] = useTable(columns, data);
  return (
    <Table striped bordered hover>
        {tableHeaders}
        {tableRows}
    </Table>
  )
}