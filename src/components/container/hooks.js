import {useState, useEffect} from 'react';
import axios from 'axios';

export const useContainer = () => {

    const [ip, SetIPs] = useState([]);
    const [data, setData] = useState([]);

    useEffect(() => {
        axios.post('http://localhost:7000/v1/ipdetails', {ip}).then(({data})=> {
            setData(data);
        }).catch(function (error) {
            console.log(error);
          });
    },[ip])

    const onChange = (ip) => {
        SetIPs(ip);
    }

    return [
        data,
        onChange
    ]
}