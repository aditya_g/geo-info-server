import { Container, Row } from 'react-bootstrap';
import {MultiInput} from '../multiInput/multiInput';
import {TableContainer} from '../table/table';
import { useContainer } from './hooks';

import 'bootstrap/dist/css/bootstrap.css';

export const MaxMindcontainer = () => {

  const [data, onChange] = useContainer();

  return(
    <Container>
        <MultiInput 
          label={"IP Address"}
          name={"ip"}
          onChange={onChange}
        />
      <Row>
        <TableContainer data={data}/>
      </Row>
    </Container>
  )
}