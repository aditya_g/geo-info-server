import {MaxMindcontainer} from './components/container/maxmindcontainer';
import './App.css';

function App() {
  return (
    <div className="App">
      <MaxMindcontainer/>
    </div>
  );
}

export default App;
