This is the user interface where we are fetching data based on the ip addresses passed on by the user:
![Screenshot_2022-04-25_at_7.10.39_PM](uploads/32f8e3ac63eac2c6dd5a5425f7320662/Screenshot_2022-04-25_at_7.10.39_PM.png)
Implementation: 
- User can pass multiple ip addresses.
- Tabular Data will be populated to the user.
- For each IP below data is populated 
      1. Country Code
      2. Postal Code
      3. City Name
      4. Time Zone
      5. Accuracy Radius
- Below is the component structure:
![Screenshot_2022-04-25_at_7.15.22_PM](uploads/38e493a72e37eb9a42c9d21c0a0f38d9/Screenshot_2022-04-25_at_7.15.22_PM.png)
- The components are structured as a reusable components.
- Application uses CRA and bundles the application using webpack.
**STEPS to run the application**
- clone the repo.
- install all NPM dependencies, npm i or npm install.
- you need to clone https://gitlab.com/aditya_g/geo-info-service since the service is not yet deployed and UI relies on local host.

Improvements:

- We can optimise the functions used further more by using callbacks and exploring bundler properties.